package main

import (
	"dnsup/internal/cli"
	"dnsup/internal/logging"
	"dnsup/pkg/dns"
	"dnsup/pkg/providers"
	"fmt"
	"github.com/spf13/pflag"
	"os"
)

var (
	logger = logging.GetInstance()
)

func main() {
	domainFlag := pflag.String("domain", "", "name of the domain to interact with; must be set here or using the environment variable DNSUP_DOMAIN")
	providerName := pflag.String("provider", "", "")
	hostname := pflag.String("hostname", "", "name of the host to use in the DNS records; if this is not set, the domain name will be used")
	updateIpv4 := pflag.Bool("ipv4", true, "update the IPv4 address for the host")
	updateIpv6 := pflag.Bool("ipv6", true, "update the IPv6 address for the host)")
	apiToken := pflag.String("api-token", "", "API token used to authenticate with the given provider")
	ttl := pflag.Int16("ttl", 3600, "")

	pflag.Usage = cli.Usage

	pflag.Parse()

	argCount := pflag.NArg()

	if argCount != cli.ArgCount {
		msg := fmt.Sprintf("Invalid number of arguments, expected %d got %d", cli.ArgCount, argCount)
		cli.MessageWithUsage(msg)

		os.Exit(cli.ExitArgError)
	}

	domain, err := cli.GetDomain(domainFlag)

	if err != nil {
		cli.MessageWithUsage("Domain is not set")

		os.Exit(cli.ExitArgError)
	}

	action := pflag.Arg(0)

	if len(*hostname) == 0 {
		err = pflag.Set("hostname", domain)

		if err != nil {
			cli.MessageWithUsage("Failed to set hostname")

			os.Exit(cli.ExitArgError)
		}
	}

	cfg := dns.Config{
		Domain:     domain,
		Hostname:   *hostname,
		Provider:   *providerName,
		UpdateIpv4: *updateIpv4,
		UpdateIpv6: *updateIpv6,
		ApiToken:   *apiToken,
	}

	provider, err := providers.NewProvider(*providerName, cfg)

	if err != nil {
		msg := fmt.Sprintf("Failed to create provider: %s", err)
		cli.MessageWithUsage(msg)

		os.Exit(cli.ExitArgError)
	}

	actions := make(chan dns.RecordActions, 2)

	if cfg.UpdateIpv4 {
		actionRecordA, err := dns.NewActionRecordA(&provider, *hostname, dns.Ttl(*ttl))

		if err != nil {
			return
		}

		actions <- actionRecordA
	}

	if cfg.UpdateIpv6 {
		actionRecordAaaa, err := dns.NewActionRecordAaaa(&provider, *hostname, dns.Ttl(*ttl))

		if err != nil {
			return
		}

		actions <- actionRecordAaaa
	}

	close(actions)

	for i := range actions {
		err = cli.RunAction(action, i)

		if err != nil {
			fmt.Printf("Action failed: %s\n", err.Error())

			os.Exit(cli.ExitActionError)
		}
	}
}
