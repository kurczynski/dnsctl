Name:       dnsup
Version:    0.0.0
Release:    %autorelease
Summary:    Update DNS records for different providers
License:    GPLv3
URL:		https://gitlab.com/kurczynski/dnsup
Source0:	%{name}-%{version}.tar.gz
BuildRequires:  golang
BuildRequires:  systemd-rpm-macros
BuildRequires:  git

%define debug_package %{nil}

%description
Update DNS records for different providers asdf

%prep
%setup -q -n %{name}-%{version}

%build
go mod tidy
GO111MODULE=on go build -v -o %{name} cmd/main.go

%install
install -Dpm 0755 %{name} %{buildroot}%{_bindir}/%{name}

%files
%{_bindir}/%{name}

%changelog
%autochangelog
