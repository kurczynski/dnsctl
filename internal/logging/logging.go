package logging

import (
	"context"
	"io"
	"log"
	"log/slog"
	"os"
	"sync"
)

var (
	loggerInstance sync.Once
	logger         *slog.Logger
)

// CliHandler handles logs in a simple way that's readable for CLI interactions
type CliHandler struct {
	handler slog.Handler
	logger  *log.Logger
}

func newCliHandler(out io.Writer, opts *slog.HandlerOptions) *CliHandler {
	return &CliHandler{
		handler: slog.NewTextHandler(out, opts),
		logger:  log.New(out, "", 0),
	}
}

func GetInstance() *slog.Logger {
	loggerInstance.Do(func() {
		logger = slog.New(newCliHandler(os.Stdout, nil))
	})

	return logger
}

func (h *CliHandler) Enabled(c context.Context, l slog.Level) bool {
	return h.handler.Enabled(c, l)
}

func (h *CliHandler) Handle(_ context.Context, r slog.Record) error {
	h.logger.Printf("%s", r.Message)

	return nil
}
func (h *CliHandler) WithAttrs(_ []slog.Attr) slog.Handler { return nil }
func (h *CliHandler) WithGroup(_ string) slog.Handler      { return nil }
