package cli

import (
	"dnsup/pkg/dns"
	"dnsup/pkg/providers"
	"fmt"
	"github.com/spf13/pflag"
	"os"
)

const (
	ExitArgError    = 1 // The application exited due to an error with the provided arguments
	ExitActionError = 2

	ArgCount = 1 // The exact number of args that must be present

	EnvDomain = "DNSUP_DOMAIN"
)

var (
	writer  = os.Stderr
	actions = map[string]func(record dns.RecordActions) error{
		"get":    dns.RecordActions.Get,
		"add":    dns.RecordActions.Add,
		"update": dns.RecordActions.Update,
		"delete": dns.RecordActions.Delete,
	}
)

func Usage() {
	binName := os.Args[0]

	msg := `
Usage of %s:
%s [domain] [provider-name], 
	[domain]		name of the domain to use
	[provider-name]		provider used to manage the DNS records, one of: %s
`
	_, _ = fmt.Fprintf(writer, fmt.Sprintf(msg, binName, binName, providers.List()))

	pflag.PrintDefaults()
}

func MessageWithUsage(msg string) {
	Message(msg)

	Usage()
}

func Message(msg string) {
	_, _ = fmt.Fprintf(writer, "%s\n", msg)
}

func GetDomain(domain *string) (string, error) {
	if len(*domain) < 1 {
		domainEnv := os.Getenv(EnvDomain)

		if len(domainEnv) < 1 {
			return "", fmt.Errorf("domain is not set")
		}

		domain = &domainEnv
	}

	return *domain, nil
}

func RunAction(action string, record dns.RecordActions) error {
	a := actions[action]

	err := a(record)

	if err != nil {
		return err
	}

	return nil
}
