package dns

import (
	"dnsup/pkg/ipaddr"
	"fmt"
	"net"
)

type RecordA struct {
	Hostname string
	Value    net.IP
	Ttl      Ttl
}

type ActionRecordA struct {
	Provider *Provider
	Record   RecordA
}

func NewActionRecordA(provider *Provider, hostname string, ttl Ttl) (RecordActions, error) {
	record := RecordA{
		Hostname: hostname,
		Ttl:      ttl,
	}

	return ActionRecordA{
		Provider: provider,

		Record: record,
	}, nil
}

func (a ActionRecordA) Get() error {
	// TODO: Finish implementing
	records, err := (*a.Provider).GetRecordsA(a.Record.Hostname)

	if err != nil {
		return fmt.Errorf("cannot get %s record: %s", TypeA, err)
	}

	fmt.Printf("%+v", records)

	return nil
}

func (a ActionRecordA) Add() error {
	ipAddr, err := ipaddr.GetIpv4()

	if err != nil {
		return fmt.Errorf("cannot get IP address for %s record: %s", TypeA, err)
	}

	a.Record.Value = ipAddr

	err = (*a.Provider).AddRecordA(a.Record)

	if err != nil {
		return fmt.Errorf("cannot add %s record: %s", TypeA, err)
	}

	return nil
}

func (a ActionRecordA) Update() error {
	ipAddr, err := ipaddr.GetIpv4()

	if err != nil {
		return fmt.Errorf("cannot get IP address for %s record: %s", TypeA, err)
	}

	a.Record.Value = ipAddr

	err = (*a.Provider).UpdateRecordA(a.Record)

	if err != nil {
		return fmt.Errorf("cannot update %s record: %s", TypeA, err)
	}

	return nil
}

func (a ActionRecordA) Delete() error {
	err := (*a.Provider).DeleteRecordA(a.Record.Hostname)

	if err != nil {
		return fmt.Errorf("cannot delete %s record: %s", TypeA, err)
	}

	return nil
}
