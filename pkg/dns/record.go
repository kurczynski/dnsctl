package dns

const (
	TypeA    = "A"
	TypeAaaa = "AAAA"
)

type Ttl uint16

type Config struct {
	Domain   string
	Hostname string
	Provider string

	UpdateIpv4 bool
	UpdateIpv6 bool
	ApiToken   string
}

type Provider interface {
	GetRecordsA(hostname string) ([]RecordA, error)
	AddRecordA(recordA RecordA) error
	UpdateRecordA(recordA RecordA) error
	DeleteRecordA(hostname string) error

	GetRecordsAaaa(hostname string) ([]RecordAaaa, error)
	AddRecordAaaa(recordAaaa RecordAaaa) error
	UpdateRecordAaaa(recordAaaa RecordAaaa) error
	DeleteRecordAaaa(hostname string) error
}

// RecordActions Actions performed on a DNS record
type RecordActions interface {
	Get() error
	Add() error
	Update() error
	Delete() error
}
