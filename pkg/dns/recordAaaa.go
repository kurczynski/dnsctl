package dns

import (
	"dnsup/pkg/ipaddr"
	"fmt"
	"net"
)

type RecordAaaa struct {
	Hostname string
	Value    net.IP
	Ttl      Ttl
}

type ActionRecordAaaa struct {
	Provider *Provider
	Record   RecordAaaa
}

func NewActionRecordAaaa(provider *Provider, hostname string, ttl Ttl) (RecordActions, error) {
	record := RecordAaaa{
		Hostname: hostname,
		Ttl:      ttl,
	}

	return ActionRecordAaaa{
		Provider: provider,

		Record: record,
	}, nil
}

func (a ActionRecordAaaa) Get() error {
	// TODO: Finish implementing
	records, err := (*a.Provider).GetRecordsAaaa(a.Record.Hostname)

	if err != nil {
		return fmt.Errorf("cannot get %s record: %s", TypeAaaa, err)
	}

	fmt.Printf("%+v", records)

	return nil
}

func (a ActionRecordAaaa) Add() error {
	ipAddr, err := ipaddr.GetIpv6()

	if err != nil {
		return fmt.Errorf("cannot get IP address for %s record: %s", TypeAaaa, err)
	}

	a.Record.Value = ipAddr

	err = (*a.Provider).AddRecordAaaa(a.Record)

	if err != nil {
		return fmt.Errorf("cannot add %s record: %s", TypeAaaa, err)
	}

	return nil
}

func (a ActionRecordAaaa) Update() error {
	ipAddr, err := ipaddr.GetIpv6()

	if err != nil {
		return fmt.Errorf("cannot get IP address for %s record: %s", TypeAaaa, err)
	}

	a.Record.Value = ipAddr

	err = (*a.Provider).UpdateRecordAaaa(a.Record)

	if err != nil {
		return fmt.Errorf("cannot update %s record: %s", TypeAaaa, err)
	}

	return nil
}

func (a ActionRecordAaaa) Delete() error {
	err := (*a.Provider).DeleteRecordA(a.Record.Hostname)

	if err != nil {
		return fmt.Errorf("cannot delete %s record: %s", TypeAaaa, err)
	}

	return nil
}
