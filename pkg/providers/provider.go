package providers

import (
	"dnsup/pkg/dns"
	"dnsup/pkg/providers/digitalocean"
	"fmt"
	"golang.org/x/exp/maps"
	"slices"
)

var (
	// list All the supported providers that can be used to update DNS records as keys and the function used to create
	// an instance as values
	list = map[string]func(dns.Config) dns.Provider{
		"digital-ocean": digitalocean.New,
	}
)

func NewProvider(provider string, cfg dns.Config) (dns.Provider, error) {
	p := list[provider]

	if p == nil {
		return nil, fmt.Errorf("provider %s is not supported", provider)
	}

	return p(cfg), nil
}

func List() []string {
	l := maps.Keys(list)
	slices.Sort(l)

	return l
}
