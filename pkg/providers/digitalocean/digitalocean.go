package digitalocean

import (
	"dnsup/pkg/dns"
	"fmt"
	"github.com/digitalocean/godo"
	"golang.org/x/net/context"
	"golang.org/x/oauth2"
	"log/slog"
	"net"
)

type DigitalOcean struct {
	cfg dns.Config

	ctx    context.Context
	client *godo.Client
}

func New(cfg dns.Config) dns.Provider {
	//	apiUrl, err := url.Parse("https://api.digitalocean.com/v2")

	token := &oauth2.Token{
		AccessToken: cfg.ApiToken,
	}
	source := oauth2.StaticTokenSource(token)

	ctx := context.Background()

	c := oauth2.NewClient(ctx, source)

	_ = godo.NewClient(c)

	return DigitalOcean{
		cfg:    cfg,
		ctx:    ctx,
		client: godo.NewFromToken(cfg.ApiToken),
	}
}

func (do DigitalOcean) AddRecordA(a dns.RecordA) error {
	req := &godo.DomainRecordEditRequest{
		Type: dns.TypeA,
		Name: a.Hostname,
		Data: a.Value.String(),
		TTL:  int(a.Ttl),
	}

	_, resp, err := do.client.Domains.CreateRecord(do.ctx, do.cfg.Domain, req)

	if err != nil {
		return err
	}

	slog.Debug("resp", resp)

	return nil
}

func (do DigitalOcean) UpdateRecordA(a dns.RecordA) error {
	req := &godo.DomainRecordEditRequest{
		Type: dns.TypeA,
		Name: a.Hostname,
		Data: a.Value.String(),
		TTL:  int(a.Ttl),
	}

	domain := do.cfg.Domain
	recordName := fmt.Sprintf("%s.%s", a.Hostname, domain)

	records, _, err := do.client.Domains.RecordsByName(do.ctx, domain, recordName, nil)

	if err != nil {
		return err
	}

	for _, i := range records {
		_, resp, err := do.client.Domains.EditRecord(do.ctx, domain, i.ID, req)

		if err != nil {
			return err
		}

		slog.Debug("resp", resp)
	}

	return nil
}

func (do DigitalOcean) DeleteRecordA(hostname string) error {
	domain := do.cfg.Domain
	recordName := fmt.Sprintf("%s.%s", hostname, domain)

	records, _, err := do.client.Domains.RecordsByName(do.ctx, domain, recordName, nil)

	if err != nil {
		return err
	}

	for _, record := range records {
		_, err = do.client.Domains.DeleteRecord(do.ctx, domain, record.ID)

		if err != nil {
			return err
		}
	}

	return nil
}

func (do DigitalOcean) GetRecordsA(hostname string) ([]dns.RecordA, error) {
	doRecords, _, err := do.client.Domains.RecordsByName(do.ctx, do.cfg.Domain, hostname, nil)

	if err != nil {
		return nil, err
	}

	records := make([]dns.RecordA, 0)

	for _, i := range doRecords {
		if i.Type == dns.TypeA {
			record := dns.RecordA{
				Hostname: i.Name,
				Value:    net.ParseIP(i.Data),
				Ttl:      dns.Ttl(i.TTL),
			}

			records = append(records, record)
		}
	}

	return records, nil
}

func (do DigitalOcean) AddRecordAaaa(a dns.RecordAaaa) error {
	req := &godo.DomainRecordEditRequest{
		Type: dns.TypeAaaa,
		Name: a.Hostname,
		Data: a.Value.String(),
		TTL:  int(a.Ttl),
	}

	_, resp, err := do.client.Domains.CreateRecord(do.ctx, do.cfg.Domain, req)

	if err != nil {
		return err
	}

	slog.Debug("resp", resp)

	return nil
}

func (do DigitalOcean) UpdateRecordAaaa(a dns.RecordAaaa) error {
	req := &godo.DomainRecordEditRequest{
		Type: dns.TypeAaaa,
		Name: a.Hostname,
		Data: a.Value.String(),
		TTL:  int(a.Ttl),
	}

	domain := do.cfg.Domain
	recordName := fmt.Sprintf("%s.%s", a.Hostname, domain)

	records, _, err := do.client.Domains.RecordsByName(do.ctx, domain, recordName, nil)

	if err != nil {
		return err
	}

	for _, i := range records {
		_, resp, err := do.client.Domains.EditRecord(do.ctx, domain, i.ID, req)

		if err != nil {
			return err
		}

		slog.Debug("resp", resp)
	}

	return nil
}

func (do DigitalOcean) DeleteRecordAaaa(hostname string) error {
	domain := do.cfg.Domain
	recordName := fmt.Sprintf("%s.%s", hostname, domain)

	records, _, err := do.client.Domains.RecordsByName(do.ctx, domain, recordName, nil)

	if err != nil {
		return err
	}

	for _, record := range records {
		_, err = do.client.Domains.DeleteRecord(do.ctx, domain, record.ID)

		if err != nil {
			return err
		}
	}

	return nil
}

func (do DigitalOcean) GetRecordsAaaa(hostname string) ([]dns.RecordAaaa, error) {
	doRecords, _, err := do.client.Domains.RecordsByName(do.ctx, do.cfg.Domain, hostname, nil)

	if err != nil {
		return nil, err
	}

	records := make([]dns.RecordAaaa, 0)

	for _, i := range doRecords {
		if i.Type == dns.TypeAaaa {
			record := dns.RecordAaaa{
				Hostname: i.Name,
				Value:    net.ParseIP(i.Data),
				Ttl:      dns.Ttl(i.TTL),
			}

			records = append(records, record)
		}
	}

	return records, nil
}
