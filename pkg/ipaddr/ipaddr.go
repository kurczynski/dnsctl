package ipaddr

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
)

const (
	ipv4Url = "https://api.ipify.org?format=json"
	ipv6Url = "https://api6.ipify.org?format=json"
)

type response struct {
	Ip net.IP `json:"ip"`
}

func GetIpv4() (net.IP, error) {
	ip, err := getIpify(ipv4Url)

	if err != nil {
		return net.IP{}, err
	}

	if ip.To4() == nil {
		return net.IP{}, fmt.Errorf("not a valid IPv4 address")
	}

	return ip, nil
}

func GetIpv6() (net.IP, error) {
	ip, err := getIpify(ipv6Url)

	if err != nil {
		return net.IP{}, err
	}

	if ip.To16() == nil {
		return net.IP{}, fmt.Errorf("not a valid IPv6 address")
	}

	return ip, nil
}

// Get public IP address from https://www.ipify.org/
func getIpify(url string) (net.IP, error) {
	res, err := http.Get(url)

	if err != nil {
		return nil, err
	}

	// FIXME
	body, err := ioutil.ReadAll(res.Body)

	if err != nil {
		return nil, err
	}

	resp := response{}

	err = json.Unmarshal(body, &resp)

	if err != nil {
		return nil, err
	}

	return net.ParseIP(resp.Ip.String()), nil
}
